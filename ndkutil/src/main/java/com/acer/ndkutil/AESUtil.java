package com.acer.ndkutil;


import android.util.Base64;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtil {

    public static String encrypt(String plainText) {

        AES aes = new AES();

        try {
            AlgorithmParameterSpec algorithmParameterSpec = new IvParameterSpec(aes.getIv1().getBytes("UTF-8"));

            SecretKeySpec secretKeySpec = new SecretKeySpec(aes.getKey1().getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, algorithmParameterSpec);

            byte[] bytes = cipher.doFinal(plainText.getBytes("UTF-8"));

            return new String(Base64.encode(bytes, Base64.NO_WRAP), "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }


    public static String decrypt(String cryptedText) {

        AES aes = new AES();

        try {
            AlgorithmParameterSpec algorithmParameterSpec = new IvParameterSpec(aes.getIv1().getBytes("UTF-8"));

            SecretKeySpec secretKeySpec = new SecretKeySpec(aes.getKey1().getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, algorithmParameterSpec);

            byte[] bytes = cipher.doFinal(Base64.decode(cryptedText, Base64.NO_WRAP));

            return new String(bytes, "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }
}
