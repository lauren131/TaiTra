package com.acer.linking.taitra.reader.others;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View.OnClickListener;
import com.acer.linking.businesstoday.fragment.OnerMainFragmentBT;
import com.acer.linking.businesstoday.others.*;
import com.acer.logger.Utils.BaseConfig;
import com.acer.newone.android.ui.BaseSectionView.onClickHandler;



/**
 * @author KevinKuo
 *
 */
public class TaiTraOnerMainFragment extends OnerMainFragmentBT implements OnClickListener, onClickHandler  {
	@Override
	protected int setTargetHeightInit(int sectionSize, int columns, int colSpan, int targetWidth) {
		return (int)(super.setTargetHeightInit(sectionSize, columns, colSpan, targetWidth) * 1.5);
	}

	@Override
	protected int setColumns() {

		Log.e("clear", "clear old");
		//clear old unencrypt data
		SharedPreferences settings = com.libs.utils.Utils.getAppContext().getSharedPreferences(BaseConfig.PREFS_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove(BaseConfig.PREFS_ONE_USER);
		editor.apply();

		editor.remove(BaseConfig.PREFS_EXTERNAL_USER);
		editor.apply();

		return Utils.isDevicePhone() ? 2 : 3;
	}
}
