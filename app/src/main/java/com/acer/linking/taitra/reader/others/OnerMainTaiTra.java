package com.acer.linking.taitra.reader.others;

import com.acer.android.fragment.OnerMainFragment;
import com.acer.linking.businesstoday.activity.OnerMainBT;

/**
 * 隨身讀首頁畫面
 * 
 * @author KevinKuo
 *
 */

public class OnerMainTaiTra extends OnerMainBT {
	@Override
	protected boolean shouldPresentVideo(){
		return false;
	}

	@Override
	protected OnerMainFragment getHomeFragment() {
		return new TaiTraOnerMainFragment();
	}
	@Override
	public ImplemetationHolder getImplemetationHolder() {
		return Utils.getImplemetationHolder();
	}
}
