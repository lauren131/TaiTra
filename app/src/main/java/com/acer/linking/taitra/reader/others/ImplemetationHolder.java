package com.acer.linking.taitra.reader.others;

import android.graphics.Color;

import com.acer.custmizeUI.fragments.ContentFragment;
import com.acer.linking.businesstoday.R;
import com.acer.linking.businesstoday.others.ROContentManagerBT;
import com.libs.utils.BaseService;

public class ImplemetationHolder extends com.acer.linking.businesstoday.others.ImplemetationHolder {
	public ImplemetationHolder() {
		super();
		setVariableStoreClass(VariableStore.class);
	}

	@Override
	public VariableStore getVariableStore() {
		return (VariableStore) super.getVariableStore();
	}

	@Override
	protected Class<? extends BaseService> getServiceType() {
		// TODO Auto-generated method stub
		return TaiTraService.class;
	}

	@Override
	public void init() {
		super.init();

		getMyNotificationManager().registerNotificationService(
				R.drawable.ic_launcher,
				Color.rgb(255, 0, 0),
				R.drawable.ic_launcher_drawer,
				5 * 60 * 1000,
				OnerMainTaiTra.class,
				this)
				.setShowLatestMessageOnly(true);
	}

	@Override
	public ROContentManagerBT getROContentManager() {
		return new TaiTraROContentManager(this);
	}

	@Override
	public boolean enableNoteFunction() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected Class<?> getNotificationActivityClass(String targetFrame, String params) {
		return OnerMainTaiTra.class;
	}

	@Override
	public ContentFragment getContentFragmentInstence() {
		return new TaiTraContentFragment();
	}
}