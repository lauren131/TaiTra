package com.acer.linking.taitra.reader.others;

import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import com.acer.android.activity.OnerMainBW;
import com.acer.android.fragment.BWContentFragment;
import com.acer.customize.*;
import com.acer.fragments.MemberFragment;
import com.acer.linking.businesstoday.fragment.ContentFragmentBT;
import com.acer.linking.businesstoday.ui.phone.ContentViewBT;
import com.acer.newone.android.ui.BaseContentView;
import com.libs.framworks.MyThread;

import org.apache.commons.lang.StringUtils;
import org.apache.http.util.TextUtils;
import org.itri.rodata.ROSectionManager;

/**
 * 是內文頁，非會員頁
 * 
 * @author KevinKuo
 *
 */
public class TaiTraContentFragment extends ContentFragmentBT {

	public TaiTraContentFragment() {
	}
    @Override
    protected BaseContentView initContentView() {
        return new TaiTraContentView(
                mActivity.get(),
                getContent(),
                highLightedString,
                getImplemetationHolder());
    }
	@Override
	public ImplemetationHolder getImplemetationHolder() {
		return (ImplemetationHolder)super.getImplemetationHolder();
	}

	@Override
	protected void setRegisterMember() {
        Integer quota = getImplemetationHolder().getROSectionManager().getFreeArticleQuota();
		MyThread tr = new MyThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				getImplemetationHolder().getROSectionManager().AcquireFreeQuota("1",true);
			}
		});
		tr.start();
		if(getImplemetationHolder().getOneConfig().getOneUser() != null && quota == null){
			try {
				tr.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			quota = getImplemetationHolder().getROSectionManager().getFreeArticleQuota();
		}

		StringBuffer SB_descTemp;
		String sstr = content.getType();
		if(TextUtils.isEmpty(sstr)) {
			sstr = authrizeGuest;
		}
		btnGobuy.setVisibility(View.VISIBLE);
        if (btnGobuy != null) {
            btnGobuy.setText("付費方案");
            btnGobuy.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callPurchasePage();
                }
            });
        }
        SB_descTemp = new StringBuffer();
		if (sstr.equals(authrizeSubscriber)) {
            if (btn_use_quota != null) {
                btn_use_quota.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(getImplemetationHolder().getOneConfig().getOneUser() == null){
                            ((OnerMainBW)getActivity()).callMemberFragment(MemberFragment.MEMBER_LOGIN);
                        }
                        else{
                            if(com.acer.customize.Utils.hasNetwork()){
                                showSimpleDialog("連結中，請稍待",
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                dismissDialog();
                                            }
                                        });
                                new MyThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        try{
                                            final ROSectionManager.ROArticleQuota result = getImplemetationHolder().getROSectionManager().AcquireFreeQuota("1",false);
                                            dismissDialog();
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {

                                                @Override
                                                public void run() {
                                                    // TODO Auto-generated method stub
                                                    if(getImplemetationHolder().getOneConfig().getOneUser() == null || "NotLogin".equals(result.messageDetail)){
                                                        ((OnerMainBW)getActivity()).callMemberFragment(MemberFragment.MEMBER_LOGIN);
                                                    } else {
                                                        if(StringUtils.isNotEmpty(result.messageDetail)){
                                                            showSimpleDialog(result.messageDetail,
                                                                    new DialogInterface.OnClickListener() {

                                                                        @Override
                                                                        public void onClick(DialogInterface dialog,
                                                                                            int which) {
                                                                            dismissDialog();
                                                                        }
                                                                    });
                                                        }
                                                    }
                                                    if(result.exceeded == 0){
                                                        TaiTraContentFragment.this.getContent().setFreeQuotaDate(result.expiredTime);
                                                        getImplemetationHolder().getROContentManager().updateLocal(TaiTraContentFragment.this.getContent(), true);
                                                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

                                                            @Override
                                                            public void run() {
                                                                // TODO Auto-generated method stub
                                                                TaiTraContentFragment.this.callRefresh();
                                                            }
                                                        },1000);
                                                    }
                                                }
                                            });

                                        }
                                        catch(Exception ex){
                                            showSimpleDialog("連線失敗，請稍後再試一次。",
                                                    new DialogInterface.OnClickListener() {

                                                        @Override
                                                        public void onClick(DialogInterface dialog,
                                                                            int which) {
                                                            dismissDialog();
                                                        }
                                                    });
                                        }
                                    }
                                }).start();


                            }
                            else{
                                showSimpleDialog("無網路連線，請開啟網路後再重試一次。",
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                dismissDialog();
                                            }
                                        });
                            }
                        }

                    }
                });
            }
		    if(v_desc_part_line != null)
		        v_desc_part_line.setVisibility(View.VISIBLE);
            if(getImplemetationHolder().getOneConfig().getOneUser() == null){
                if (btn_use_quota != null) {
                    btn_use_quota.setVisibility(View.VISIBLE);
                    btn_use_quota.setText(" 會員登入 ");
                    btn_use_quota.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callMemberPage();
                        }
                    });
                }
                SB_descTemp.append("您尚未登入會員，請先登入會員，會員每月可免費閱讀4篇文章。付費會員則可無限閱讀！");
            } else if(quota == null){
                SB_descTemp.append("請點擊下方免費閱讀以解鎖文章");
            } else if(quota != null && quota == 0){
                SB_descTemp.append("您這個月的免費閱讀已達上限，請加入付費會員，可無限閱讀");
                if(ll_Ad_Title != null){
                    ll_Ad_Title.setVisibility(View.GONE);
                }
                if(btnGobuy != null){
                    btnGobuy.setVisibility(View.VISIBLE);
                }
                if(btn_use_quota != null){
                    btn_use_quota.setVisibility(View.GONE);
                }
            }
            else{
                SB_descTemp.append("您的免費閱讀文章數尚餘"+quota+"篇");
                if(ll_Ad_Title != null){
                    ll_Ad_Title.setVisibility(View.GONE);
                }
                if(btnGobuy != null){
                    btnGobuy.setVisibility(View.GONE);
                }
                if(btn_use_quota != null){
                    btn_use_quota.setVisibility(View.VISIBLE);
                }
                if (btn_use_quota != null) {
                    btn_use_quota.setText("閱讀本篇文章");
                }
            }
		} else if (sstr.equals(authrizeMember)) {
			SB_descTemp.append("本文限會員觀看");
            btn_use_quota.setText(" 會員登入 ");
            btn_use_quota.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					callMemberPage();
				}
			});
		} else {
			btnGobuy.setVisibility(View.GONE);
		}

        tvAdDesc.setText(SB_descTemp.toString());
        tvAdDesc.setGravity(Gravity.LEFT);

		updateADImage();
		if (ivCustomorizedAD != null) {
			ivCustomorizedAD.setVisibility(View.VISIBLE);
		}
	}
}
