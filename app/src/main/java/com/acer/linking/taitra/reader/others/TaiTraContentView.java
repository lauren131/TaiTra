package com.acer.linking.taitra.reader.others;

import android.annotation.SuppressLint;
import android.app.Activity;

import com.acer.linking.businesstoday.others.*;
import com.acer.linking.businesstoday.ui.phone.ContentViewBT;

import org.itri.rodata.ROContent;

/**
 * Created by watttsai on 2018/8/9.
 */

public class TaiTraContentView extends ContentViewBT {
    @SuppressLint("SetJavaScriptEnabled")
    public TaiTraContentView(Activity context, ROContent content, String highLightedString, ImplemetationHolder implemetationHolder){
        super(context, content, highLightedString,implemetationHolder);
    }

    @Override
    protected boolean isEnableClickImage() {
        return true;
    }
}
